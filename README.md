**Tópicos**

[[_TOC_]]
# **Conceitos básicos de Big data** #
 Big data é o termo utilizado para referenciar as grandes quantidades de dados geradas todos os dias, isso também é conhecido como os três Vs,  todos os tipos de aplicações geram um dado, até um simples clique é armazenado para ser analisado em busca de insights para melhores decisões e movimentos estratégicos de negócios. ###

# **Para que serve?** #
 Ele se trata de uma tecnológia com o objetivo de conquistar vantagem competitiva, proporcionando para as empresas organizarem,lerem e interpretarem dados de alta qualidade a respeito de seu produto ou serviço, após a leitura dos dados pode se entender de forma assertiva várias questões e pontos críticos do seu negócio, desde a experiência do cliente ou até mesmo uma futura tendência. ####

# **Conhecendo mais os 3Vs** #

| Volume |
| ------- |
| Como visto anteriormante, big data é referente a uma gigante quantidade de dados armazenados, sendo alguns dados de valor desconhecido e baixa densidade, como feeds de dados do Twitter,fluxo de cliques em uma página web ou até mesmo um equipamento habilitado para sensores, para algumas organizações isso pode utilizar dezenas de terabyes, já para outras centenas de petabytes. |

| Velocidade |
| ------ |
| Esse item é referente a grande velocidade de dados gerados diariamente, além das mídias socias temos inúmeras transições sendo feitas, compras por cartões de crédito, aquisições de ações, análises de flutuações de câmbio de moedas internacionais, entre muitas outras, cada processo gera preciosos dados, que podem ser solucionados instantaneamente sem a necessidade de armazenamento da big data.|

| Variedade |
| ------ |
| O big data possui uma grandiosa variedade de informações, e alguns novos tipos de dados vem em formatos não estruturados e semiestruturados exigindo assim um pré-processamento para a obtenção de um significado, tais como:  
* Imagens, fotos, ilustrações, captura de imagem etc;
* Dados de reconhecimentos faciais;
* Áudios;
* Dados produzidos por dispositivos via IoT (Internet das coisas).

**Valor e Veracidade**

Com o passar do tempo e o avanço tecnológico foram criadas mais duas partições, **_valor_** referente as ações geradas para um négocio ou seja precisam ser informações que agreguem valor, para que os gestores possam melhorar suas decisões, e **_veracidade_** que é nada mais a qualidade dos dados, como os dados vem de diferentes fontes e formas, é difícil para que as empresas combinem eles para uma boa interpretação, parecidos com processos de validação e conferência de dados, as big datas conseguem entregar dados mais confiáveis e verídicos, por meio de relatórios, estatísticas e análises, com base em grandes volumes de informações. ####

# **Tecnologias para tratamento de volumes de dados** #

## SGBD's ##
Data Base Management System ou Sistema de Gerenciamento de Banco de Dados, é um software responsável por controlar, acessar, organizar e proteger as informações de alguma aplicação em um banco de dados, é uma maneira de armazenar todos os dados empresariais em um único local, provendo mais segurança para as informações e facilitando suas consultas.

**Tipos de SGBD's**
| Relacionais |
| ------- |
|Banco de dados em formatos de tabelas, que podem se relacionar, cada tabela possui diferentes atributos, com diversos tipos de dados. |

| Não-relacionais |
| ------- |
|Nesse formato de banco de dados, normalmente não é utilizado o SQL como linguagem de consulta, e possuem diversos tipos de dados como, documentos gráficos, chave-valor e colunares.|

| Hierárquico |
| ------- |
|Consiste em uma coleção de arquivos conectados entre si por meio de ligações, o bando de dados se baseia em um modelo de entidades e relacionamentos. |

| De rede |
| ------- |
|Semelhante a organização do Hierárquico, sendo sua maior diferença a isenção de restrição hierárquica, e permite organização em várias listas, formada por um conjunto complexo de ligações. |

| Orientado a objetos |
| ------- |
|Neste modelo, as funcionalidades da orientação a objetos se relacionam ao banco de dados, onde as informações são ligadas a um objeto e seus registros em formatos de tuplas. |

## Mensageria ##
Simplificado, o sistemas de troca de mensagem ou mensageria, nada mais é do que automação do envio de notas fiscais por parte das empresas e toda sua relação com o Fisco, o intuito dessa ferramenta é evitar riscos causados pelas pessoas, evitando assim multas contra a empresa.

Apesar do sistema fiscal brasileiro possuir muitas complexidades, um sistema de ERP da empresa integrado com o sistema de mensageria facilita sua administração, geralmente seu funcionamento segue um padrão, o ERP gera a nota fiscal e a mensageria envia o arquivo para o orgão fiscal e o fisco confirma o recebimento dos arquivos.

Seu funcionamento serve tanto para a emissão quanto para recepção de documentos.

## ETL ##
ETL, cujo significado é extração, transformação e carregamento (Loading), consiste em um processo de integração de dados ou seja, os dados são extraídos de um sistema-fonte, sendo então transformados em um formato de mais claro entendimento para que possam ser analizados e armazenados em um armazém ou outro sistema.

O ETL, funciona em conjunto com outras ferramentas de intregração de dados, e de várias maneiras diferentes como, data quality, data governance, virtualização e metadados.

**Aqui estão 3 exemplos:** 

| Usos convencionais |
| ------ |
| Muitas organizações dependem de seu uso todos os dias, varejistas que precisam de uma clara visualização dos dados das vendas, operadoras de saúde que procuram um quadro preciso do seu uso, combinar e exibir dados de uma data warehouse e outros bancos de dados, migração de antigos sistemas para mais modernos entre outros usos. |

| Big Data |
| ------ |
|Fundamental para as empresas que desejam se manter no topo, afinal ter um fácil acesso a uma grande quantidade de dados podem dar a elas vantagem competitiva, o ETL atualmente recebe várias transformações para que possa ter acesso a todos os tipos de dados, vídeos, mídias socias, Internet das coisas. |

| Data Quality |
| ------ |
|O ETL e outras ferramenta de integração de dados, são usadas para a limpeza, perfilação e auditar dados, tornando-os mais confiáveis, ele se integra as ferramentas de data quality, e os fornecedores de ETL incorporam soluções referentes como mapeamento de linhagem de dados. |
